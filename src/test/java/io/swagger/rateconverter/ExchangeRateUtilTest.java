package io.swagger.rateconverter;

import io.swagger.api.ApiException;
import io.swagger.model.ExchangeRateInput;
import io.swagger.model.ModelApiResponse;
import org.junit.BeforeClass;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Currency;

import static org.junit.Assert.*;

public class ExchangeRateUtilTest {


    static ExchangeRateUtil exchangeRateUtil;
    @BeforeClass
    public static void setUp() throws Exception {

        exchangeRateUtil = new ExchangeRateUtil();

        exchangeRateUtil.addCurrencyRate(Currency.getInstance("USD"), new BigDecimal(1.2));
        exchangeRateUtil.addCurrencyRate(Currency.getInstance("GBP"), new BigDecimal(0.90));
        exchangeRateUtil.addCurrencyRate(Currency.getInstance("CHF"), new BigDecimal(1.11));
        exchangeRateUtil.addCurrencyRate(Currency.getInstance("JPY"), new BigDecimal(1000.01));
    }


    @Test
    public void test_source_Currency_3_digits_code_ISO_4217_check() {

        ExchangeRateInput input = new ExchangeRateInput();
        input.setSourceCurrency("USD");
        input.setTargetCurrency("EUR");
        input.setSourceAmmount(new BigDecimal( 12));

        boolean valid = exchangeRateUtil.isValidCurrencyCode(input.getSourceCurrency());

        assertTrue(valid);
    }

    @Test
    public void test_target_Currency_3_digits_code_ISO_4217_check() {

        ExchangeRateInput input = new ExchangeRateInput();
        input.setSourceCurrency("USD");
        input.setTargetCurrency("EUR");
        input.setSourceAmmount(new BigDecimal( 12));

        boolean valid = exchangeRateUtil.isValidCurrencyCode(input.getTargetCurrency());

        assertTrue(valid);
    }


    @Test
    public void testExchange() {

        ExchangeRateInput input = new ExchangeRateInput();
        input.setSourceCurrency("USD");
        input.setTargetCurrency("EUR");
        input.setSourceAmmount(new BigDecimal( 12));

         ModelApiResponse reponse =  exchangeRateUtil.exchange(input);

         assertTrue(reponse.getExchangeAmmount().equals("€10.00"));

    }

    @Test
    public void testExchange_fixed_two_decimal_format () {

        ExchangeRateInput input = new ExchangeRateInput();
        input.setSourceCurrency("USD");
        input.setTargetCurrency("EUR");
        input.setSourceAmmount(new BigDecimal( 50));

        ModelApiResponse reponse =  exchangeRateUtil.exchange(input);

        assertTrue(reponse.getExchangeAmmount().endsWith(".00"));

    }

    @Test(expected = ApiException.class)
    public void testWrongSourceCurrency () {

        ExchangeRateInput input = new ExchangeRateInput();
        input.setSourceCurrency("USZ");
        input.setTargetCurrency("EUR");
        input.setSourceAmmount(new BigDecimal( 12));

        exchangeRateUtil.isValidCurrencyCode(input.getSourceCurrency());

    }

    @Test(expected = ApiException.class)
    public void testWrongTargetCurrency () {

        ExchangeRateInput input = new ExchangeRateInput();
        input.setSourceCurrency("USD");
        input.setTargetCurrency("EUO");
        input.setSourceAmmount(new BigDecimal( 12));

        exchangeRateUtil.isValidCurrencyCode(input.getTargetCurrency());
    }
}