package io.swagger.api;

public enum ApplicationErrorCode {

    NOT_FOUND(1001,"Does not exist");

    private ApplicationErrorCode(int code1, String errorDesc)
   {
       setCode(code1);
       setErrorMessage(errorDesc);
   }
   private int code;
   private String errorMessage;


    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
