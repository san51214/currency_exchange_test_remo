package io.swagger.api;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-02-07T00:04:04.077Z")

public class ApiException extends RuntimeException{
    private int code;
    public ApiException (int code, String msg) {
        super(msg);
        this.code = code;
    }
    public ApiException(ApplicationErrorCode errorCode)
    {
        super(errorCode.getErrorMessage());
        this.code = errorCode.getCode();
    }
    public ApiException(ApplicationErrorCode errorCode, String message)
    {
        super("{"+errorCode+"}"+message);
        this.code = errorCode.getCode();
    }

}
