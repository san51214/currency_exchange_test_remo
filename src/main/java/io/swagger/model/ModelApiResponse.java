package io.swagger.model;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.math.BigDecimal;
import org.springframework.validation.annotation.Validated;
import javax.validation.Valid;
import javax.validation.constraints.*;

/**
 * ModelApiResponse
 */
@Validated
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2019-02-07T00:04:04.077Z")

public class ModelApiResponse   {
  @JsonProperty("sourceCurrency")
  private String  sourceCurrency = null;

  @JsonProperty("sourceAmmount")
  private BigDecimal sourceAmmount = null;

  @JsonProperty("targetCurrency")
  private String  targetCurrency = null;

  @JsonProperty("exchangeAmmount")
  private String exchangeAmmount = null;

  public ModelApiResponse sourceCurrency(String  sourceCurrency) {
    this.sourceCurrency = sourceCurrency;
    return this;
  }

  /**
   * Get sourceCurrency
   * @return sourceCurrency
  **/
  @ApiModelProperty(value = "")


  public String  getSourceCurrency() {
    return sourceCurrency;
  }

  public void setSourceCurrency(String  sourceCurrency) {
    this.sourceCurrency = sourceCurrency;
  }

  public ModelApiResponse sourceAmmount(BigDecimal sourceAmmount) {
    this.sourceAmmount = sourceAmmount;
    return this;
  }

  /**
   * Get sourceAmmount
   * @return sourceAmmount
  **/
  @ApiModelProperty(value = "")

  @Valid

  public BigDecimal getSourceAmmount() {
    return sourceAmmount;
  }

  public void setSourceAmmount(BigDecimal sourceAmmount) {
    this.sourceAmmount = sourceAmmount;
  }

  public ModelApiResponse targetCurrency(String  targetCurrency) {
    this.targetCurrency = targetCurrency;
    return this;
  }

  /**
   * Get targetCurrency
   * @return targetCurrency
  **/
  @ApiModelProperty(value = "")


  public String  getTargetCurrency() {
    return targetCurrency;
  }

  public void setTargetCurrency(String  targetCurrency) {
    this.targetCurrency = targetCurrency;
  }

  public ModelApiResponse exchangeAmmount(String exchangeAmmount) {
    this.exchangeAmmount = exchangeAmmount;
    return this;
  }

  /**
   * Get exchangeAmmount
   * @return exchangeAmmount
  **/
  @ApiModelProperty(value = "")


  public String getExchangeAmmount() {
    return exchangeAmmount;
  }

  public void setExchangeAmmount(String exchangeAmmount) {
    this.exchangeAmmount = exchangeAmmount;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    ModelApiResponse _apiResponse = (ModelApiResponse) o;
    return Objects.equals(this.sourceCurrency, _apiResponse.sourceCurrency) &&
        Objects.equals(this.sourceAmmount, _apiResponse.sourceAmmount) &&
        Objects.equals(this.targetCurrency, _apiResponse.targetCurrency) &&
        Objects.equals(this.exchangeAmmount, _apiResponse.exchangeAmmount);
  }

  @Override
  public int hashCode() {
    return Objects.hash(sourceCurrency, sourceAmmount, targetCurrency, exchangeAmmount);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class ModelApiResponse {\n");
    
    sb.append("    sourceCurrency: ").append(toIndentedString(sourceCurrency)).append("\n");
    sb.append("    sourceAmmount: ").append(toIndentedString(sourceAmmount)).append("\n");
    sb.append("    targetCurrency: ").append(toIndentedString(targetCurrency)).append("\n");
    sb.append("    exchangeAmmount: ").append(toIndentedString(exchangeAmmount)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }
}

