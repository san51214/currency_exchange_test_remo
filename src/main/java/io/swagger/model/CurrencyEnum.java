package io.swagger.model;

import java.util.Currency;
import java.util.Locale;

public enum CurrencyEnum {
    USD("USD",Currency.getInstance("US Dollor")),EUR("EURO",Currency.getInstance("EUR")),
            GBP("Great British Pound",Currency.getInstance("GBP")),
    CHF("Swiss Franc",Currency.getInstance("CHF")),
    JYP("Japani Yen",Currency.getInstance("JYP"));

    private String title;
    private Currency currency;
    private CurrencyEnum(String title, Currency currncy)
    {
        this.title = title;
        this.currency = currncy;
    }
}
