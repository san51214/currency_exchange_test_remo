package io.swagger.rateconverter;

import io.swagger.api.ApiException;
import io.swagger.api.ApplicationErrorCode;
import io.swagger.model.ExchangeRateInput;
import io.swagger.model.ModelApiResponse;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Currency;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class ExchangeRateUtil {

    Map<Currency,BigDecimal> euroRateMap = new HashMap<>();

    public void addCurrencyRate(Currency currency, BigDecimal rate)
    {
        euroRateMap.put(currency,rate);
    }

    /**
     * return the excahnge rate ammount with source and target currency
     * @param input
     * @return
     */
    public ModelApiResponse exchange(ExchangeRateInput input)
    {

        isValidCurrencyCode(input.getSourceCurrency());

        isValidCurrencyCode(input.getTargetCurrency());

        Currency srcCurrency = Currency.getInstance( input.getSourceCurrency() );
        Currency targetCurrency = Currency.getInstance( input.getTargetCurrency() );
        BigDecimal ammount = input.getSourceAmmount();

        BigDecimal euroForSrcCurrency = ammount.divide( euroRateMap.get(srcCurrency), RoundingMode.FLOOR);

        BigDecimal toTargetFromEuro = input.getTargetCurrency().equals("EUR") ? euroForSrcCurrency : ( euroForSrcCurrency.divide( euroRateMap.get(targetCurrency) , RoundingMode.FLOOR));

        ModelApiResponse modelResponse = new ModelApiResponse();

        modelResponse.setSourceCurrency(input.getSourceCurrency());
        modelResponse.setTargetCurrency(input.getTargetCurrency());
        modelResponse.setSourceAmmount(input.getSourceAmmount());
        modelResponse.setExchangeAmmount(targetCurrency.getSymbol()+toTargetFromEuro.setScale(2, RoundingMode.FLOOR));

        return modelResponse;
    }

    /**
     * validate the currency code
     * @param currencyCode
     * @return
     */
    public boolean isValidCurrencyCode(String currencyCode)
    {
        Optional<Currency> targetCurrencyExists = Currency.getAvailableCurrencies().stream().
                filter(c -> c.getCurrencyCode().equals(currencyCode)).
                findFirst();
        if(targetCurrencyExists.orElse(null)==null)
        {
            throw new ApiException(ApplicationErrorCode.NOT_FOUND.getCode(),"Currency:"+currencyCode+" not found");
        }

        return true;
    }

}
